"""
No exemplo a baixo, o proxy server utiliza socket protocol
para receber callbacks de novos links requesitados
pelo client.

@class RCOM
@author Martim Martins <martim13artins13@gmail.com>
"""
import asyncio
import logging
import aiohttp
import requests

from enum import Enum
from typing import Optional, Type, TypeVar

T = TypeVar("T")
R = TypeVar("R", bound="Route")

HOST = "0.0.0.0"
PORT = 8888
log = logging.getLogger("Proxy-Server")


class HTTPMethods(Enum):
    GET: str = "GET"


class MessageType(Enum):
    NULL: bytes = b""


class Route:
    def __init__(self, method: str, url: str, host: str, protocol: str):
        self.method: str = method
        self.url: str = url
        self.path: Optional[str] = url.split("/")
        self.host: str = host
        self.protocol: str = protocol

    @classmethod
    def from_str(cls, data: str) -> Type[R]:
        lines = data.splitlines()
        data = lines[0].split(" ")
        host = lines[1].split(" ")[1]

        return cls(
            method=data[0],
            url=data[1],
            protocol=data[2],
            host=host,
        )


class Request:
    def __init__(self) -> None:
        ...

    @classmethod
    def from_str(self, data: str):
        for line in data.splitlines():
            ...


class Proxy:
    def __init__(
        self, loop: asyncio.AbstractEventLoop, session: aiohttp.ClientSession = None
    ):
        self.loop: asyncio.AbstractEventLoop = loop

    async def connect(self, host: str):
        host, port = host.split(":")
        family = (await self.loop.getaddrinfo(host, port))[0][0]

        await asyncio.open_connection(host=host, port=port, family=family)

    async def request(self, route: T, **kw):
        ...

    async def run(self):
        server = await asyncio.start_server(self.on_socket_new, HOST, PORT)
        log.info(
            "Proxy server está pronto para receber requests em `%s:%s`.", HOST, PORT
        )

        try:
            await server.serve_forever()
        finally:
            server.close()
            await server.wait_closed()

    async def on_socket_new(
        self, reader: asyncio.StreamReader, writer: asyncio.StreamWriter
    ):
        # Receber data do cliente de até 2048 bytes
        data = await reader.read(2048)

        if data == MessageType.NULL:
            writer.close()
            return

        await self.on_data_received(data, writer)

    async def on_data_received(self, data: bytes, writer: asyncio.StreamWriter):
        data = data.decode("utf-8")
        print(data)
        route: Route = Route.from_str(data)
        
        if route.method == "GET":
            requests.get(route.url)

        await self.connect(route.host)
        writer.write(f"{route.method} {route.path} {route.protocol}\n".encode("utf-8"))


if __name__ == "__main__":
    # Configurar logging
    logging.basicConfig(
        level=logging.DEBUG,
        format="[%(asctime)s] [%(levelname)s] [%(name)s] - %(message)s",
    )

    loop = asyncio.get_event_loop()
    loop.run_until_complete(Proxy(loop).run())
