# Projeto Infraestrutura de rede
Iremos fazer o projeto de infraestrutura de rede do [DigitalOcean](https://www.digitalocean.com/).

A Digital Ocean é uma plataforma de computação em nuvem que fornece infraestrutura como serviço, dando aos programadores e às outras empresas uma forma simples de criar e gerir as suas aplicações.
Ela oferece compute instances, armazenamento de informação e banda larga aos seus clientes.

## Switch ToR 
Foi escolhido para switch ToR, o [Cisco Nexus 9332 ACI Leaf Switch w 32P 40G QSFP](https://www.connection.com/product/cisco-nexus-9332-aci-leaf-switch-w-32p-40g-qsfp/n9k-c9332pq/18118613), ele contem 36 portas. Este switch permite até 2.56 terabits por segundo de bandwidth interna.

Foi escolhido este router em especifico porque é barato, têm 36 portas, e permite spine-leaf architecture, que é necessário para os servidores conseguirem comunicarem entre si.

Preço por unidade
PVD: 21,155.94 €

## Servidor de Rack
Foi escolhido para servidor de rack, o [Dell PowerEdge R6525](https://www.dell.com/pt/empresas/p/poweredge-r6525/pd), com 32 ranhuras RDIMM, com 12 unidades SAS/SATA/NVMe, dois processadores CPU [AMD EPYC™ de 3.ª Geração](https://www.amd.com/pt/partner/3rd-gen-epyc-processors-business-performance-revolution) com até 64 núcleos por processador e com sistema operativo [Ubuntu 18.04.6 LTS](https://releases.ubuntu.com/18.04/).

Escolhemos este servidor porque suporta muita memoria ram e contem muitos núcleos por processador.

Preço por unidade (*Com ram, cpu, disks, power supply*)
PVD: 96,391.89 €

![Img](https://i.dell.com/is/image/DellContent//content/dam/global-site-design/product_images/dell_enterprise_products/enterprise_systems/poweredge/r6525/pdp/poweredge-r6525-amd-fullwidth-pdp.jpg?fmt=jpg&wid=965&hei=554)
> Figura que mostra o servidor de rack.


### CPU
O servidor vem com CPU [AMD EPYC 7H12 2.60GHz, 64C/128T, 256M Cache (280W) DDR4-3200](https://www.amd.com/en/products/cpu/amd-epyc-7h12).

Preço por unidade
*Já incluído no preço do servidor rack.*

### Memory
O servidor ira conter 32 sticks de 32GB RDIMM, 3200MT/s, Dual Rank.

Preço por unidade
*Já incluído no preço do servidor rack.*

### Disks
O servidor ira conter 7 discos de 7.68TB, Enterprise, NVMe, Read Intensive, U2, G4, P5500 with carrier,
que é equivalente a 53,76TB.

Preço por unidade
*Já incluído no preço do servidor rack.*

### Power supply
O servidor ira conter Dual, Hot-plug, Fully Redundant Power Supply, 1400W.

Preço por unidade
*Já incluído no preço do servidor rack.*

## DR
Foi escolhido para DR (disaster recovery), [Barracuda Networks Backup Server](https://www.senetic.pt/product/BBSI1091A), que contem capacidade total de armazenamento de 102 TB

Preço por unidade
PVD: 115,788.20 €

## Rack
O rack que ira conter os servidores e switch ToR será o [server rack without doors](https://www.rackmountsolutions.net/kendall-howard-3180-3-001-37-37u-linier-open-frame-server-rack-no-doors-36-depth/), pois tem 37 rows, ou seja, pode conter 1 switch ToR  e 36 switches, que são a quantidade de 
portas que o switch ToR tem.

Preço por unidade
PVD: 793.94 €

## Sistema de Refrigeração
O sistema de refrigeração dos servidores será constituído pelos [kits ventilação rack 4 ventoinhas](https://cablematic.com/pt/produtos/kit-ventilacao-rack-19-1u-4-ventoinhas-de-120mm-rackmatic-RK064/?cr=EUR&ct=PT#extra_product_info).

Preço por unidade 
PVD: 76.11 €

## ISP
A Internet service provider escolhida foi a [AT&T](https://www.business.att.com/products/att-dedicated-internet.html) 10gb por segundo, porque oferece boa estabilidade de serviço e bastante banda larga.

# Diagramas
Nos diagramas seguintes está representado toda a infraestrutura de rede da impresa digital ocean.

## Estrutura dos clusters
![Img1](https://cdn.discordapp.com/attachments/890972826931195944/951236185303425024/Servers.png)

## Back-end da digital ocean
![Img2](https://i.imgur.com/TEn0MzD.png)

## Serviços a rodar no servidores 
![Img3](https://i.imgur.com/YvCSyUV.png)

## Estabelecimento do datacenter
![Img4](https://i.imgur.com/XihRlnI.png)
