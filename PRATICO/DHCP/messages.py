"""
Todos os tipo de messagens que iram ser trocadas entre o servidor
e o cliente.

@author martim13artins13@gmail.com
@class RCOM
"""

from typing import TypedDict

class ACK(TypedDict):
	...